# Hermes Helpdesk
#### Video Demo: https://youtu.be/AfCY7AwhbN8
#### Description:
The Hermes Helpdesk is a comprehensive ticketing system designed to manage and track various tasks or issues within an organization. This project leverages modern technologies such as Flask, SQLAlchemy, PostgreSQL, Docker, and cryptography to provide a secure and efficient solution for task management. The following sections will delve deeper into the design choices, features, and functionalities of the Hermes Helpdesk.

##### Project Overview

The primary goal of the Hermes Helpdesk is to streamline the process of managing tasks and issues within an organization. By providing a centralized platform for ticket creation, assignment, tracking, and collaboration, the system aims to improve efficiency, transparency, and accountability in handling organizational tasks.

##### Design Choices

The Hermes Helpdesk is built using Flask as the primary web framework due to its simplicity, flexibility, and extensive community support. SQLAlchemy is used as an Object-Relational Mapping (ORM) tool to facilitate seamless interaction with the PostgreSQL database. Cryptography techniques are employed to ensure data security, particularly for sensitive information such as user passwords.

Docker containers encapsulate the entire application stack, including the web server, database, and any other dependencies, allowing for easy deployment and scaling in various environments. Database migrations are managed using Alembic, ensuring consistent schema changes across different environments.

##### Features and Functionalities

The Hermes Helpdesk offers several key features to support efficient task management:

1. **Ticket Management**: Users can create, view, update, and close tickets representing individual tasks or issues. Each ticket can have multiple comments associated with it, enabling collaboration and progress tracking.
2. **User Authentication and Authorization**: The system supports user authentication and authorization, ensuring that only authorized users can access specific features based on their roles and permissions.
3. **Data Encryption**: Sensitive data, such as user passwords, are encrypted using hashing and salting techniques to protect against unauthorized access.
4. **Database Migration Management**: Alembic is used to manage database schema changes consistently across different environments.
5. **Containerization**: Docker containers encapsulate the entire application stack, enabling easy deployment and scaling in various environments.

##### Future Development Plans

The Hermes Helpdesk has several future development plans to further enhance its functionality and usability:

1. **Assigning Tickets to Staff**: Implement a feature that allows tickets to be assigned to specific staff members, ensuring accountability and efficient task distribution.
2. **User Creation by Support Staff**: Enable support staff to create new users within the system, streamlining user management processes.
3. **Client Front End Development**: Develop a simple client front end to provide an intuitive interface for managing tickets and collaborating on tasks.

The Hermes Helpdesk is designed with scalability, security, and efficiency in mind. By leveraging modern technologies and best practices, the system aims to provide organizations with a reliable and secure ticketing solution that can grow and adapt to their evolving needs.


TODOS
- [ ] Allow tickets to be assigned to staff by staff
- [ ] Allow support staff to create new users
- [ ] Create simple client front end