#!/bin/bash

SESSION="hermes_session"

tmux has-session -t $SESSION 2>/dev/null

if [ $? != 0 ]; then
	tmux new-session -d -s $SESSION -n "editor"
	tmux send-keys -t $SESSION:editor "nvim README.org" C-m

	tmux new-window -t $SESSION -n "docker"
	tmux send-keys -t $SESSION:docker "docker compose up -d" C-m
	tmux send-keys -t $SESSION:docker "watch -n 1 docker compose logs" C-m

	tmux select-window -t $SESSION:editor
fi




tmux attach-session -t $SESSION
