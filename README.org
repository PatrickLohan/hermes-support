* Hello, World!
Hermes Support is a ticketing app like no other! (citations needed)
A user will be able to log in and create tickets, and comment on tickets they own. If they are also support staff, they will be able to assign a ticket to any staff member, and comment on those tickets.
** Overview
Things needed to create the API and database are listed here.
*** FastAPI
#+begin_src bash
$ pip install "fastapi[all]"
#+end_src
*** PostgreSQL
Using docker
*** SQLAlchemy (and ORM package)
*** Alembic
*** Docker
**** Permissions
- data folder
start the containers with compose, then stop and run `sudo chown -R 1000:1000 ./data` and restart
*** Development
- Clone this repository
- Make sure you have `docker compose` installed
- run `run.sh` to start the postgres container, then start the project with debugpy attached
- in neovim run dap and attach remotely to the default settings (127.0.0.1, 5678)
