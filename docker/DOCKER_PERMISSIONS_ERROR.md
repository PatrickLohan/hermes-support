# Permissions errors with postgres docker image

## Temporary solution

1. start the container using your normal docker-compose file, this creates the
directory with the hardcoded uid:gid (999:999)

``` yaml
version: '3.7'

services:
  db:
    image: postgres
    container_name: postgres
    volumes:
      - ./data:/var/lib/postgresql/data
    environment:
      POSTGRES_USER: fake_database_user
      POSTGRES_PASSWORD: fake_database_PASSWORD
```

2. stop the container and manually change the ownership to uid:gid you
want (I'll use 1000:1000 for this example)

``` bash
docker stop postgres
sudo chown -R 1000:1000 ./data 
```

3. Edit your docker file to add your desired uid:gid and start it up again using
docker-compose (notice the user:)

``` yaml
version: '3.7'

services:
  db:
    image: postgres
    container_name: postgres
    volumes:
      - ./data:/var/lib/postgresql/data
    user: 1000:1000
    environment:
      POSTGRES_USER: fake_database_user
      POSTGRES_PASSWORD: fake_database_password
```

The reason you can't just use user: from the start is that if the image runs as
a different user it fails to create the data files.

## Real solution

1. Create database folder in project root, and refactor app folder to be
named api (make sure all imports follow this change)
2. Create a dockerfile in the database folder

``` dockerfile
FROM postgres:12

# Create the needed temp file before the first postgreSQL execution

RUN mkdir temp

# Create group and user

RUN groupadd non-root-postgres-group
RUN useradd non-root-postgres-user --group non-root-postgres-group

# Set user rights to allow the on-root-postgres-user 
# to access the temp folder

RUN chown -R non-root-postgres-user:non-root-postgres-group /temp
RUN chmod 777 /temp

# Change to non-root privilege

USER non-root-postgres
```

3. change the docker compose code to reflect building from this new dockerfile

### Source

[https://suedbroecker.net/2020/06/23/run-a-postgressql-container-as-a-non-root-user-in-openshift/](https://suedbroecker.net/2020/06/23/run-a-postgressql-container-as-a-non-root-user-in-openshift/)
