-- Only run if tables don't exist
DO $$
BEGIN
    IF NOT EXISTS (SELECT FROM pg_tables WHERE tablename = 'users') THEN
        -- Create tables first
        CREATE TABLE users (
            id SERIAL PRIMARY KEY,
            email VARCHAR NOT NULL UNIQUE,
            password VARCHAR NOT NULL,
            created_at TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL,
            support_staff BOOLEAN DEFAULT FALSE
        );

        CREATE TABLE tickets (
            id SERIAL PRIMARY KEY,
            title VARCHAR NOT NULL,
            description VARCHAR NOT NULL,
            published BOOLEAN DEFAULT TRUE NOT NULL,
            created_at TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL,
            owner_id INTEGER NOT NULL REFERENCES users(id) ON DELETE CASCADE
        );

        CREATE TABLE comments (
            id SERIAL PRIMARY KEY,
            owner_id INTEGER REFERENCES users(id) ON DELETE CASCADE,
            ticket_id INTEGER REFERENCES tickets(id) ON DELETE CASCADE,
            description VARCHAR NOT NULL,
            created_at TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL
        );

        -- Then insert the data
        INSERT INTO users (email, password, support_staff) VALUES
        ('admin@hermes.com', '$2b$12$Y1dTi0lolQaSKLZWQ7SOo.9.KMlLqxDWjw8X.NMBtoCCg9vCsletC', true),
        ('user@hermes.com', '$2b$12$Y1dTi0lolQaSKLZWQ7SOo.9.KMlLqxDWjw8X.NMBtoCCg9vCsletC', false);

	   -- Tickets
	   INSERT INTO tickets (title, description, owner_id) VALUES
	   ('Test Ticket 1', 'This is a test ticket', 2),
	   ('Test Ticket 2', 'Another test ticket', 2);

	   -- Comments
	   INSERT INTO comments (description, owner_id, ticket_id) VALUES
	   ('Support response', 1, 1),
	   ('User follow-up', 2, 1);
	 END IF;
END $$;
