"""Seed admin user in database

Revision ID: 8d963f4962a5
Revises:
Create Date: 2024-08-03 23:13:46.101490

"""

from typing import Sequence, Union

from seed_db import create_admin, delete_admin

# revision identifiers, used by Alembic.
revision: str = "8d963f4962a5"
down_revision: Union[str, None] = None
branch_labels: Union[str, Sequence[str], None] = None
depends_on: Union[str, Sequence[str], None] = None


def upgrade() -> None:
    delete_admin()
    create_admin()


def downgrade() -> None:
    delete_admin()
