import datetime

from .database import Base
from sqlalchemy import Integer, String, Boolean, TIMESTAMP, text, ForeignKey
from sqlalchemy.orm import relationship, mapped_column, Mapped


class Ticket(Base):
    """
    Represents a ticket.
    Attributes:
        id (int): The unique identifier for the ticket.
        title (str): The title of the ticket.
        description (str): The content of the ticket.
        published (bool): Indicates whether the ticket is published or not.
        created_at (datetime): The timestamp when the ticket was created.
        owner_id (int): The ID of the user who owns the ticket.
        owner (User): The user who owns the ticket.
    """

    __tablename__ = "tickets"
    id: Mapped[int] = mapped_column(Integer, primary_key=True, nullable=False)
    title: Mapped[str] = mapped_column(String, nullable=False)
    description: Mapped[str] = mapped_column(String, nullable=False)
    published: Mapped[bool] = mapped_column(Boolean, server_default="TRUE", nullable=False)
    created_at: Mapped[datetime.datetime] = mapped_column(
        TIMESTAMP(timezone=True), nullable=False, server_default=text("now()")
    )
    owner_id: Mapped[int] = mapped_column(Integer, ForeignKey("users.id", ondelete="CASCADE"), nullable=False)
    owner = relationship("User")
    comments = relationship("Comment", back_populates="ticket", cascade="all, delete-orphan")


class User(Base):
    __tablename__ = "users"
    id: Mapped[int] = mapped_column(Integer, primary_key=True, nullable=False)
    email: Mapped[str] = mapped_column(String, nullable=False, unique=True)
    password: Mapped[str] = mapped_column(String, nullable=False)
    created_at: Mapped[datetime.datetime] = mapped_column(
        TIMESTAMP(timezone=True), nullable=False, server_default=text("now()")
    )
    support_staff: Mapped[bool] = mapped_column(Boolean, server_default="FALSE")


class Comment(Base):
    __tablename__ = "comments"
    id: Mapped[int] = mapped_column(Integer, primary_key=True, nullable=False)
    owner_id: Mapped[int] = mapped_column(Integer, ForeignKey("users.id", ondelete="CASCADE"))
    ticket_id: Mapped[int] = mapped_column(Integer, ForeignKey("tickets.id", ondelete="CASCADE"))
    description: Mapped[str] = mapped_column(String, nullable=False)
    created_at: Mapped[datetime.datetime] = mapped_column(
        TIMESTAMP(timezone=True), nullable=False, server_default=text("now()")
    )
    ticket = relationship("Ticket", back_populates="comments")
