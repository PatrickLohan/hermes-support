from pydantic_settings import BaseSettings
from dotenv import find_dotenv, load_dotenv

load_dotenv(find_dotenv(".env"))


class ConfigSettings(BaseSettings):
    database_driver: str
    database_username: str
    database_password: str
    api_hostname: str
    database_hostname: str
    database_name: str
    database_port: str
    secret_key: str
    algorithm: str
    access_token_expire_minutes: int

    class Config:
        env_file = "../docker/.env"


settings = ConfigSettings()
