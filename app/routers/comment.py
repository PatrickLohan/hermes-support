from fastapi import APIRouter, Depends, status, HTTPException
from sqlalchemy.orm import Session
from .. import database, models, oauth2, schemas

router = APIRouter(prefix="/comments", tags=["Comments"])


@router.post(
    path="/{ticket_id}",
    status_code=status.HTTP_201_CREATED,
    response_model=schemas.TicketOut,
)
async def create_comment(
    ticket_id: int,
    comment: schemas.CommentCreate,
    db: Session = Depends(database.get_db),
    current_user: schemas.UserOut = Depends(oauth2.get_current_user),
) -> schemas.TicketOut:
    """
    Comment on a ticket. Support staff will be able to comment on any ticket whereas
    non-support staff will only be able to comment on their own tickets.

    :param ticket_id: The ID of the ticket to comment on.
    :param new_comment: The comment to be added to the ticket.
    :param db: The database session.
    :param current_user: The currently logged in user.
    :return: The ticket and comments.
    """

    ticket: models.Ticket = (db.query(models.Ticket).where(models.Ticket.id == ticket_id)).first()
    if not ticket:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"Ticket with id {ticket_id} does not exist",
        )

    # TODO only allow support staff to comment on any ticket. Otherwise only allow user to comment on tickets they own.
    if not current_user.support_staff and ticket.owner_id is not current_user.id:
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail="Not authorised to perform requested action")

    new_comment = models.Comment(owner_id=current_user.id, ticket_id=ticket_id, **comment.dict())

    db.add(new_comment)
    db.commit()
    db.refresh(new_comment)

    return ticket
