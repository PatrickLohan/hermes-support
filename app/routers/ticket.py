# from datetime import datetime, timezone
from typing import List

from fastapi import Depends, HTTPException, Response, status, APIRouter
from sqlalchemy.orm import Session

from .. import models, schemas, oauth2
from ..database import get_db
from ..schemas import UserOut

router = APIRouter(prefix="/tickets", tags=["Tickets"])


@router.get("/{ticket_id}", response_model=schemas.TicketOut)
def get_ticket(
    ticket_id: int,
    db: Session = Depends(get_db),
    current_user: UserOut = Depends(oauth2.get_current_user),
):
    # ticket = db.query(models.Ticket).filter(models.Ticket.id == ticket_id).first()
    ticket = db.query(models.Ticket).group_by(models.Ticket.id).first()

    if not ticket:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"Ticket {ticket_id} was not found",
        )
    # NOTE if we want to restrict a user to only seeing a ticket they wrote
    if ticket.owner_id != current_user.id:
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN,
            detail="Not authorised to perform requested action",
        )

    return ticket


@router.put("/{ticket_id}")
def update_ticket(
    ticket_id: int,
    updated_ticket: schemas.TicketCreate,
    db: Session = Depends(get_db),
    current_user: UserOut = Depends(oauth2.get_current_user),
):
    ticket = db.query(models.Ticket).filter(models.Ticket.id == ticket_id)

    if ticket is None:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"Ticket with id {ticket_id} was not found",
        )
    if ticket.owner_id != current_user.id:
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN,
            detail="Not authorised to perform requested action",
        )

    ticket_query.update(updated_ticket.dict(), synchronize_session=False)
    db.commit()

    updated_ticket = (
        db.query(models.Ticket).filter(models.Ticket.id == ticket_id).first()
    )

    return updated_ticket


@router.delete("/{ticket_id}", status_code=status.HTTP_204_NO_CONTENT)
def delete_ticket(
    ticket_id: int,
    db: Session = Depends(get_db),
    current_user: UserOut = Depends(oauth2.get_current_user),
):
    ticket_query = db.query(models.Ticket).filter(models.Ticket.id == ticket_id)
    ticket = ticket_query.first()

    if ticket is None:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"Ticket with id {ticket_id} was not found",
        )
    if ticket.owner_id != current_user.id:
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN,
            detail="Not authorised to perform requested action",
        )

    ticket_query.delete(synchronize_session=False)
    db.commit()

    return Response(status_code=status.HTTP_204_NO_CONTENT)


@router.post("/", status_code=status.HTTP_201_CREATED, response_model=schemas.Ticket)
def create_ticket(
    ticket: schemas.TicketCreate,
    db: Session = Depends(get_db),
    current_user: UserOut = Depends(oauth2.get_current_user),
) -> schemas.Ticket:
    if not ticket.title or not ticket.description:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Either title or description was empty.",
        )

    # so let's unpack
    new_ticket = models.Ticket(owner_id=current_user.id, **ticket.dict())

    db.add(new_ticket)
    db.commit()
    db.refresh(new_ticket)
    return new_ticket


@router.get("/", response_model=List[schemas.Ticket])
def get_tickets(
    db: Session = Depends(get_db),
    current_user: UserOut = Depends(oauth2.get_current_user),
):
    tickets: List[schemas.TicketOut] = []
    if current_user.support_staff:
        tickets = db.query(models.Ticket).group_by(models.Ticket.id).all()
    else:
        tickets = (
            db.query(models.Ticket)
            .filter(models.Ticket.owner_id == current_user.id)
            .group_by(models.Ticket.id)
            .all()
        )

    return tickets
