from sqlalchemy import create_engine
from sqlalchemy.orm import DeclarativeBase, sessionmaker
from .config import settings

DB_URL = f"{settings.database_driver}://{settings.database_username}:{settings.database_password}@{settings.database_hostname}:{settings.database_port}/{settings.database_name}"
engine = create_engine(DB_URL)

SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)


# Base = DeclarativeBase
class Base(DeclarativeBase):
    pass


def get_db():
    """
    This function is a dependency that provides a database session. It uses a
    Python generator to yield a Session object.

    The try-finally block ensures that the session is properly closed after
    use, regardless of whether an exception occurred.
    """
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()
