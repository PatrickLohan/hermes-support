from datetime import datetime
from typing import List, Optional
from pydantic import BaseModel, EmailStr


class TicketBase(BaseModel):
    title: str
    description: str
    published: bool = True

    class Config:
        from_attributes = True


class TicketCreate(TicketBase):
    pass


class UserOut(BaseModel):
    id: int
    email: EmailStr
    created_at: datetime
    support_staff: bool = False

    class Config:
        from_attributes = True


class Ticket(TicketBase):
    id: int
    created_at: datetime
    owner_id: int
    owner: UserOut

    class Config:
        from_attributes = True


class CommentBase(BaseModel):
    description: str

    class Config:
        from_attributes = True


class CommentOut(CommentBase):
    ticket_id: int
    owner_id: int

    class Config:
        from_attributes = True


class CommentCreate(CommentBase):
    pass

    class Config:
        from_attributes = True


class TicketOut(Ticket):
    comments: List[CommentOut] | None

    class Config:
        from_attributes = True


class UserCreate(BaseModel):
    email: EmailStr
    password: str
    support_staff: bool = False

    class Config:
        from_attributes = True


class UserLogin(BaseModel):
    email: EmailStr
    password: str

    class Config:
        from_attributes = True


class Token(BaseModel):
    access_token: str
    token_type: str

    class Config:
        from_attributes = True


class TokenData(BaseModel):
    id: Optional[int] = None

    class Config:
        from_attributes = True


# class CommentBase(BaseModel):
#     status: Annotated[int, Field(strict=True, ge=-1, le=1)]
