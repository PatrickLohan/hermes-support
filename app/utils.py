# May need to install dependencies in a container image
import bcrypt


def hash_password(password: str) -> str:
    """
    This function takes a plain text password, hashes it and return the hashed version.
    """
    return bcrypt.hashpw(password.encode("utf-8"), bcrypt.gensalt()).decode("utf-8")


def verify_password(plain_password: str, hashed_password: str) -> bool:
    """
    This function takes a plain text password and a hashed password, hashes the plain text password, and
    returns True if it matches the hashed password and False otherwise.
    """
    return bcrypt.checkpw(
        plain_password.encode("utf-8"), hashed_password.encode("utf-8")
    )
