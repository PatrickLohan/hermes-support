"""Missing docstring."""

# import uvicorn  # include this and the if __name__ for debugging, then run ./run.sh
from fastapi import FastAPI
from app import database, models
from app.routers import auth, comment, ticket, user
from fastapi.middleware.cors import CORSMiddleware


models.Base.metadata.create_all(bind=database.engine)

app = FastAPI()

origins = [
    "http://localhost:5173",
]
# CorsMiddleware false error
# https://github.com/tiangolo/fastapi/discussions/10968
# noinspection PyTypeChecker
app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

app.include_router(auth.router)
app.include_router(comment.router)
app.include_router(ticket.router)
app.include_router(user.router)


@app.get("/")
def pong():
    """Hello, World."""
    return {"message": "Hello, World! Welcome to your Hermes Helpdesk."}


# if __name__ == "__main__":
#     uvicorn.run(app, host="0.0.0.0", port=8000)
