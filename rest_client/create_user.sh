#!/bin/bash

URL=localhost:8000/users
USER=$1
DOMAIN=$2
PASSWD=$3
IS_STAFF=$4
http --json --follow $URL email="$USER"@"$DOMAIN" password="$PASSWD" support_staff:="$IS_STAFF"


