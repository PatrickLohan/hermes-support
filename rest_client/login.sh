#!/bin/bash
#
# Check if the correct number of arguments were provided
if [ $# -ne 3 ]; then
  echo "Usage: $0 <user> <domain> <password>"
  exit 1
fi

URL=localhost:8000/login
USER=$1
DOMAIN=$2
PASSWD=$3
http --form $URL username="$USER"@"$DOMAIN" password="$PASSWD" > response.json
