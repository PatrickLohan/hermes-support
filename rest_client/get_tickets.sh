#!/bin/bash

URL=localhost:8000/tickets
TOKEN=$(jq -r ".access_token" response.json)
http --follow -A bearer -a $TOKEN GET $URL
