#!/bin/bash

# Check if the correct number of arguments were provided
if [ $# -ne 2 ]; then
  echo "Usage: $0 <title> <description>"
  exit 1
fi

URL=localhost:8000/tickets
TOKEN=$(jq -r ".access_token" response.json)
TITLE=$1
DESCRIPTION=$2
http --follow -A bearer -a "$TOKEN" POST $URL title="$TITLE" description="$DESCRIPTION" published=true

