#!/bin/bash

URL=localhost:8000/tickets/$1
TOKEN=$(jq -r ".access_token" response.json)
http --follow -A bearer -a $TOKEN GET $URL
