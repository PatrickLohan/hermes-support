#!/bin/bash

URL=localhost:8000/comments/$1
TOKEN=$(jq -r ".access_token" response.json)
DESCRIPTION=$2
http --follow -A bearer -a $TOKEN POST $URL description="$DESCRIPTION"
